import 'package:flutter/material.dart';

class AppColors {
  AppColors._(); // this basically makes it so you can instantiate this class

  static const Map<int, Color> theme = const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFF2B4F50),
    200: const Color(0xFF2B4F50),
    300: const Color(0x000000),
    400: const Color(0xFF2B4F50),
    500: const Color(0xFF2B4F50),
    600: const Color(0xFF2B4F50),
    700: const Color(0xFF2B4F50),
    800: const Color(0xFF2B4F50),
    900: const Color(0xFF2B4F50)
  };
  static const Map<int, Color> black_new = const <int, Color>{
    50: const Color(0xFF8D8D8D),
  };
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF101112);
  static const Color black_72 = Color(0x20101112);
  static const Color lightgrey = Color(0xFFF7F7F7);
  static const Color bgGrey = Color(0xFFEAEAEA);
  static const Color vlightgrey = Color.fromARGB(10, 51, 51, 51);
  static const Color cancel_bg = Color.fromARGB(12, 235, 67, 71);
  static const Color grey = Color(0xFFCECECE);
  static const Color red = Color(0xFFEB4347);
  static const Color orange = Color(0xFFEB9F01);
  static const Color pink = Color(0xFFE91E63);
  static const Color blue = Color(0xFF2F80ED);
  static const Color violet = Color(0xff50289C);
  static const Color crimson = Color(0xFFEC5845);
  static const Color green = Color(0xFF006064);
}
