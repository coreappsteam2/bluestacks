import 'package:app_developer_assignment/services/service.dart';

class Category {
  String name;
  String image;
  int id;


  Category.parseJson(Map json) {
    id = json['id'];
    name = json['name'];
    image = Service.imageBase + (json['image']??"");

  }
}
