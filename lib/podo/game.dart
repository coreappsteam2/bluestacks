class Game {
  String name;
  String cover_url;
  String game_format;

  Game(Map json) {
    this.name = json['game_name'];
    this.cover_url = json['cover_url'];
    this.game_format = json['game_format'];
  }
}
