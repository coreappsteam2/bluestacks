import 'package:app_developer_assignment/services/service.dart';

class User {
  String name;
  String image;
  String email;
  String mobile;
  String partner_id,agent_id;
  int id;


  User.parseJson(Map json) {
    id = json['id'];
    name = json['fullName'];
    image = Service.imageBase + (json['profile_pic']??"");
    email = json['email'];
    mobile = json['phone'];
    partner_id = json['partner_id'];
    agent_id = json['agent_id'];
  }
}
