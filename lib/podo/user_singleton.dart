import 'package:app_developer_assignment/services/service.dart';

class UserSingleton {
  String name;
  String image;
  String email;
  String mobile;
  int tournamentWon;
  int winningPercentage;
  String tournamentWonDisplay;
  String referral_code;
  int partner_id,agent_id;
  int id;
  UserSingleton._privateConstructor();

  static final UserSingleton _instance = UserSingleton._privateConstructor();

  static UserSingleton get instance => _instance;

  UserSingleton.parseJson(Map json) {
    UserSingleton.instance.id = json['id'];
    UserSingleton.instance.name = json['fullName'];
    UserSingleton.instance.image = Service.imageBase + (json['profile_pic']??"");
    UserSingleton.instance.email = json['email'];
    UserSingleton.instance.mobile = json['phone'];
    UserSingleton.instance.partner_id = json['partner_id'];
    UserSingleton.instance.agent_id = json['agent_id'];
    UserSingleton.instance.referral_code = json['referral_code'];
//    UserSingleton.instance.tournamentWon = json['tournaments_won'] ?? 0;
//    UserSingleton.instance.winningPercentage = json['winning_percentage'] ?? 0;
//    var tournamentWon = UserSingleton.instance.tournamentWon;
//    UserSingleton.instance.tournamentWonDisplay = tournamentWon > 9
//        ? tournamentWon.toString()
//        : "0" + tournamentWon.toString();
  }
}
