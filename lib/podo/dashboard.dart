import 'package:app_developer_assignment/services/service.dart';

class Dashboard{
  String name;
  String image;
  String referral_code;
  String pending_sales;
  String total_orders,total_partners_added,total_agents_added,total_sales,total_earnings,total_products;


  Dashboard.parseJson(Map json) {
    name = json['fullName'];
    image = Service.imageBase + (json['image']??"");
    referral_code = json['referral_code'];
    pending_sales = json['pending']!=null?json['pending'].toString():"0";
    total_orders = json['orders']!=null?json['orders'].toString():"0";
    total_partners_added = json['partners_added']!=null?json['partners_added'].toString():"0";
    total_agents_added = json['agents_added']!=null?json['agents_added'].toString():"0";
    total_sales = json['total_sale']!=null?json['total_sale'].toString():"0";
    total_earnings = json['margin']!=null?json['margin'].toString():"0";
    total_products = json['total_products']!=null?json['total_products'].toString():"0";
  }
}