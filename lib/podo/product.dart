import 'package:app_developer_assignment/services/service.dart';

class Product {
  String title;
  String image;
  int id;
  int favorite;
  int mrp;
  int margin;
  int qty=0;

  Product.parseJson(Map json) {
    id = json['id'];
    favorite = json['favourite'];
    title = json['title'];
    margin = json['margin'];
    mrp = json['mrp'];
    qty = json['qty']!=null?json['qty']:0;
    image = Service.imageBase + (json['image']??"");

  }
}
