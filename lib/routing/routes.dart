import 'package:app_developer_assignment/ui/auth/login.dart';
import 'package:app_developer_assignment/ui/home/cart_list.dart';
import 'package:app_developer_assignment/ui/home/category_list.dart';
import 'package:app_developer_assignment/ui/home/customer_list.dart';
import 'package:app_developer_assignment/ui/home/home.dart';
import 'package:app_developer_assignment/ui/home/order_list.dart';
import 'package:app_developer_assignment/ui/home/partner_list.dart';
import 'package:app_developer_assignment/ui/home/product_list.dart';
import 'package:app_developer_assignment/ui/partner_add/add_customer.dart';
import 'package:app_developer_assignment/ui/partner_add/add_partner.dart';
import 'package:flutter/material.dart';

class Routes {
  Routes._();

  static const String login = '/login';
  static const String home = '/home';
  static const String addPartner = '/addPartner';
  static const String addCustomer = '/addCustomer';
  static const String partnerList = '/partnerList';
  static const String customerList = '/customerList';
  static const String orderList = '/orderList';
  static const String categoryList = '/categoryList';
  static const String productList = '/productList';
  static const String cartList = '/cartList';

  static final routes = <String, WidgetBuilder>{
    login: (BuildContext context) => LoginScreen(),
    home: (BuildContext context) => HomeScreen(),
    addPartner: (BuildContext context) => AddPartnerScreen(),
    addCustomer: (BuildContext context) => AddCustomerScreen(),
    partnerList: (BuildContext context) => PartnerList(),
    customerList: (BuildContext context) => CustomerList(),
    categoryList: (BuildContext context) => CategoryList(),
    productList: (BuildContext context) => ProductList(),
    cartList: (BuildContext context) => CartList(),
    orderList: (BuildContext context) => OrderList(),
  };
}
