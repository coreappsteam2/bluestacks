import 'package:app_developer_assignment/data/repository.dart';
import 'package:app_developer_assignment/di/modules/local_module.dart';
import 'package:app_developer_assignment/di/modules/preference_module.dart';
import 'package:inject/inject.dart';

import '../../main.dart';
import 'app_component.inject.dart' as g;

/// The top level injector that stitches together multiple app features into
/// a complete app.
@Injector(const [LocalModule, PreferenceModule])
abstract class AppComponent {
  @provide
  MyApp get app;

  static Future<AppComponent> create(
    LocalModule localModule,
    PreferenceModule preferenceModule,
  ) async {
    return await g.AppComponent$Injector.create(
      localModule,
      preferenceModule,
    );
  }

  /// An accessor to RestClient object that an application may use.
  @provide
  Repository getRepository();
}
