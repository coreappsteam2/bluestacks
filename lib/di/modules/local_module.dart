import 'package:app_developer_assignment/data/repository.dart';
import 'package:app_developer_assignment/data/sharedpref/shared_preference_helper.dart';
import 'package:inject/inject.dart';

@module
class LocalModule {
  // DI variables:--------------------------------------------------------------

  LocalModule() {}

  // DI Providers:--------------------------------------------------------------

  /// A singleton repository provider.
  ///
  /// Calling it multiple times will return the same instance.
  @provide
  @singleton
  Repository provideRepository(SharedPreferenceHelper preferenceHelper) =>
      Repository(preferenceHelper);
}
