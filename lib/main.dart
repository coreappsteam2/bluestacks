import 'package:app_developer_assignment/generated/l10n.dart';
import 'package:app_developer_assignment/routing/routes.dart';
import 'package:app_developer_assignment/stores/language/language_store.dart';
import 'package:app_developer_assignment/ui/splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'di/components/app_component.dart';
import 'di/modules/local_module.dart';
import 'di/modules/preference_module.dart';

// global instance for app component
AppComponent appComponent;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark // status bar color
      ));

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
  ]).then((_) async {
    appComponent = await AppComponent.create(
      LocalModule(),
      PreferenceModule(),
    );
    /*await CallKeep.setup();*/
    runApp(appComponent.app);
  });
}

class MyApp extends StatelessWidget {
  SharedPreferences prefs;
  final LanguageStore _languageStore =
      LanguageStore(appComponent.getRepository());

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<LanguageStore>.value(value: _languageStore),
        ],
        child: Observer(builder: (context) {
          return MaterialApp(
            routes: Routes.routes,
            localizationsDelegates: [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: S.delegate.supportedLocales,
            locale: Locale(_languageStore.locale),
            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              primarySwatch: Colors.blueGrey,
            ),
            home: SplashScreen(),
          );
        }));
  }
}
