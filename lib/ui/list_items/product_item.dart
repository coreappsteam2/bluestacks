import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/generated/l10n.dart';
import 'package:app_developer_assignment/podo/category.dart';
import 'package:app_developer_assignment/podo/game.dart';
import 'package:app_developer_assignment/podo/product.dart';
import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../routing/routes.dart';
import '../../services/service.dart';
import '../../utilities/utilities.dart';

class ProductListItemWidget extends StatelessWidget {
  final Product product;

  ProductListItemWidget({this.product});

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(top: 5),
    child: GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, Routes.productList);
      },
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Container(
                decoration: new BoxDecoration(
                  color: AppColors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 15.0, bottom: 15.0, left: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        child: ClipRRect(

                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          child: Image(image: new NetworkImage(product.image),height: 50,width: 50,),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Container(
                                width: MediaQuery.of(context).size.width * .5,
                                child: SimpleTextWidget(
                                  text: product.title,
                                  fontWeight:FontWeight.bold,
                                  color: AppColors.black,
                                  size: 19,
                                  textAlign: TextAlign.left,
                                  wrap: false,
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                                child: SimpleTextWidget(
                                  text: "Customer Price : ₹ "+product.mrp.toString(),
                                  color: AppColors.black.withOpacity(0.7),
                                  size: 16,
                                  textAlign: TextAlign.left,
                                  wrap: false,
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(

                                child: SimpleTextWidget(
                                  text: "Your Margin : ₹ "+int.parse((double.parse((product.mrp*(product.margin/100)).toString()).toStringAsFixed(0))).toString(),
                                  color: AppColors.green,
                                  size: 16,
                                  textAlign: TextAlign.left,
                                  wrap: false,
                                )),
                          ),
                        ],
                      ),
                      GestureDetector(
                        onTap: (){
                          markFavorite(product);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            product.favorite==1?Icons.favorite:Icons.favorite_border,
                            color: AppColors.red,
                          ),
                        ),
                      )
                    ],
                  ),
                )),
            Container(
              margin: const EdgeInsets.all(2.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: [
                  GestureDetector(
                    onTap: (){

                    },
                    child: Container(
                      child: Icon(Icons.remove_circle),
                    ),
                  ),
                  SimpleTextWidget(
                      text: "${product.qty}",
                      size: 20.0,
                      fontWeight: FontWeight.bold,
                      color: AppColors.black),
                  GestureDetector(
                    onTap: (){
                      product.qty--;
                    },
                    child: Container(
                      child: Icon(Icons.add_circle),
                    ),
                  )
                ],

              ),
            )
          ],
        ),
      ),
    ),
  );

  markFavorite(Product product){

    Service apiSetup = Service();

    apiSetup.favoriteProduct(
      product_id:this.product.id.toString(),
      type:"1",
      success: (obj) {


      },
      fail: (message) {
        Utilities.showToast(message, false);
      },
    );
  }
}
