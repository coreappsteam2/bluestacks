import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/podo/category.dart';
import 'package:app_developer_assignment/podo/game.dart';
import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/ui/home/product_list.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../routing/routes.dart';

class CategoryListItemWidget extends StatelessWidget {
  final Category category;

  CategoryListItemWidget({this.category});

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(top: 5),
    child: GestureDetector(
      onTap: (){
        ProductListState.category_id=this.category.id.toString();
        Navigator.pushNamed(context, Routes.productList);
      },
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Container(
                decoration: new BoxDecoration(
                  color: AppColors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 15.0, bottom: 15.0, left: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        child: ClipRRect(

                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          child: Image(image: new NetworkImage(category.image),height: 50,width: 50,),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0,top: 9.0),
                              child: Container(
                                  width: MediaQuery.of(context).size.width * .5,
                                  child: SimpleTextWidget(
                                    text: category.name,
                                    color: AppColors.black,
                                    size: 18,
                                    textAlign: TextAlign.left,
                                    wrap: false,
                                  )),
                            ),
                          ),
                         ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.chevron_right,
                          color: AppColors.grey,
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
      ),
    ),
  );
}
