import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/podo/game.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartListItemWidget extends StatelessWidget {
  final Game game;

  CartListItemWidget({this.game});

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(top: 5),
    child: Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25)),
            child: Image(image: new NetworkImage(game.cover_url)),
          ),
          Container(
              decoration: new BoxDecoration(
                color: AppColors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25)),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 15.0, bottom: 15.0, left: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Container(
                              width: MediaQuery.of(context).size.width * .7,
                              child: SimpleTextWidget(
                                text: game.name,
                                color: AppColors.black,
                                size: 18,
                                textAlign: TextAlign.left,
                                wrap: false,
                              )),
                        ),
                        Padding(
                          padding:
                          const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                          child: Container(
                              child: SimpleTextWidget(
                                text: game.game_format,
                                color: AppColors.black.withOpacity(0.8),
                                size: 14,
                              )),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.chevron_right,
                        color: AppColors.grey,
                      ),
                    )
                  ],
                ),
              )),
        ],
      ),
    ),
  );
}
