import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/podo/game.dart';
import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/services/service.dart';
import 'package:app_developer_assignment/ui/home/order_list.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../routing/routes.dart';

class CustomerListItemWidget extends StatelessWidget {
  final User user;

  CustomerListItemWidget({this.user});

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(top: 5),
    child: GestureDetector(
      onTap: (){

      },
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Container(
                decoration: new BoxDecoration(
                  color: AppColors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 15.0, bottom: 15.0, left: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        child: ClipRRect(

                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          child: Image(image: new NetworkImage(user.image),height: 50,width: 50,),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Container(
                                width: MediaQuery.of(context).size.width * .3,
                                child: SimpleTextWidget(
                                  text: user.name,
                                  color: AppColors.black,
                                  size: 18,
                                  textAlign: TextAlign.left,
                                  wrap: false,
                                )),
                          ),
                          Padding(
                            padding:
                            const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                            child: Container(
                                child: SimpleTextWidget(
                                  text: user.mobile,
                                  color: AppColors.black.withOpacity(0.8),
                                  size: 14,
                                )),
                          ),

                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                            onTap: (){
                              OrderListState.user_id=this.user.id.toString();
                              Navigator.pushNamed(context, Routes.orderList);
                            },
                            child: Container(
                              width: 100,
                              margin: EdgeInsets.fromLTRB(0, 5, 10, 10),
                              color: AppColors.pink.withOpacity(0.8),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: SimpleTextWidget(
                                  text: "View Orders",
                                  color: AppColors.white,
                                  size: 16,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){
                              OrderListState.user_id=this.user.id.toString();
                              Navigator.pushNamed(context, Routes.cartList);
                            },
                            child: Container(
                              width: 100,
                              color: AppColors.orange.withOpacity(0.8),
                              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: SimpleTextWidget(
                                  text: "View Cart",
                                  color: AppColors.white,
                                  size: 16,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )),

          ],
        ),
      ),
    ),
  );
}
