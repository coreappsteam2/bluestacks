import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/podo/game.dart';
import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/ui/home/order_list.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../routing/routes.dart';

class OrderListItemWidget extends StatelessWidget {
  final User user;

  OrderListItemWidget({this.user});

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(top: 5),
    child: GestureDetector(
      onTap: (){
        OrderListState.user_id=this.user.id.toString();
        Navigator.pushNamed(context, Routes.orderList);
      },
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Container(
                decoration: new BoxDecoration(
                  color: AppColors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 15.0, bottom: 15.0, left: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        child: ClipRRect(

                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          child: Image(image: new NetworkImage("https://via.placeholder.com/50"),height: 50,width: 50,),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Container(
                                width: MediaQuery.of(context).size.width * .5,
                                child: SimpleTextWidget(
                                  text: user.name,
                                  color: AppColors.black,
                                  size: 18,
                                  textAlign: TextAlign.left,
                                  wrap: false,
                                )),
                          ),
                          Padding(
                            padding:
                            const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                            child: Container(
                                child: SimpleTextWidget(
                                  text: user.mobile,
                                  color: AppColors.black.withOpacity(0.8),
                                  size: 14,
                                )),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.chevron_right,
                          color: AppColors.grey,
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
      ),
    ),
  );
}
