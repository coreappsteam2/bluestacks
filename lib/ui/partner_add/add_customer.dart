import 'dart:developer';

import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/data/sharedpref/constants/preferences.dart';
import 'package:app_developer_assignment/generated/l10n.dart';
import 'package:app_developer_assignment/routing/routes.dart';
import 'package:app_developer_assignment/stores/language/language_store.dart';
import 'package:app_developer_assignment/ui/widgets/rounded_button_widget.dart';
import 'package:app_developer_assignment/ui/widgets/textfield_widget.dart';
import 'package:app_developer_assignment/utilities/utilities.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/service.dart';
import '../widgets/simple_text_widget.dart';

class AddCustomerScreen extends StatefulWidget {
  @override
  _AddCustomerScreenState createState() => _AddCustomerScreenState();
}

class _AddCustomerScreenState extends State<AddCustomerScreen> {
  LanguageStore _languageStore;
  TextEditingController _userUserFullnameController = TextEditingController();
  TextEditingController _userUserMobileController = TextEditingController();
  TextEditingController _userUserEmailController = TextEditingController();
  SharedPreferences prefs;
  ProgressDialog progressDialog = new ProgressDialog();
  final _formKey = GlobalKey<FormState>();
  bool validated = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // initializing stores
    _languageStore = Provider.of<LanguageStore>(context);
  }

  @override
  void initState() {
    super.initState();
    loadVariables();
  }

  loadVariables() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.pink,
        elevation: 0,
        title: SimpleTextWidget(
            text: S.of(context).addCustomer,
            size: 18.0,
            fontWeight: FontWeight.bold,
            color: AppColors.white),
        /*actions: [
          GestureDetector(
            onTap: () {
              changeLanguage();
            },
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                    height: 25,
                    width: 25,
                    color: AppColors.white,
                    image: AssetImage('assets/images/change_language.jpg')),
              ),
            ),
          )
        ],*/
      ),
      body: Container(
        color: AppColors.white,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Form(
          key: _formKey,
          onChanged: () =>
              setState(() => validated = _formKey.currentState.validate()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _buildUserFullnameField(),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _buildUserEmailField(),
              ),SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _buildUserMobilelField(),
              ),
              SizedBox(height: 50,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _buildSaveButton(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildUserEmailField() {
    return TextFieldWidget(
      hint: S.of(context).enterUserEmail,
      maxLength: 50,
      marginLeft: 0,
      inputType: TextInputType.text,
      textController: _userUserEmailController,
      inputAction: TextInputAction.next,
      icon: Icon(
        Icons.email,
        color: AppColors.grey,
      ),
      validator: (value) {

        return null;
      },
    );
  }
  Widget _buildUserFullnameField() {
    return TextFieldWidget(
      hint: S.of(context).enterUserFullname,
      maxLength: 50,
      marginLeft: 0,
      inputType: TextInputType.text,
      textController: _userUserFullnameController,
      inputAction: TextInputAction.next,
      icon: Icon(
        Icons.person,
        color: AppColors.grey,
      ),
      validator: (value) {
        if (value == null || value.isEmpty || value.length < 3) {
          return S.of(context).userfullnameError;
        }
        return null;
      },
    );
  }
  Widget _buildUserMobilelField() {
    return TextFieldWidget(
      hint: S.of(context).enterUserMobile,
      maxLength: 50,
      marginLeft: 0,
      inputType: TextInputType.number,
      textController: _userUserMobileController,
      inputAction: TextInputAction.next,
      icon: Icon(
        Icons.phone,
        color: AppColors.grey,
      ),
      validator: (value) {
        if (value == null || value.isEmpty || value.length < 3) {
          return S.of(context).userMobileError;
        }
        return null;
      },
    );
  }


  Widget _buildSaveButton() {
    return RoundedButtonWidget(
      buttonText: S.of(context).save,
      textColor: AppColors.white,
      buttonColor: AppColors.pink,
      onPressed: validated ? (() => callRegisterAPI(context)) : null,
    );
  }

  callRegisterAPI(context) async {


    Service apiSetup = Service();

    apiSetup.createCustomer(
      mobile:_userUserMobileController.text.trim(),
      email:_userUserEmailController.text.trim(),
      fullName:_userUserFullnameController.text.trim(),
      success: (obj) {
        Navigator.pop(context);

      },
      fail: (message) {
        Utilities.showToast(message, false);
      },
    );
  }

  changeLanguage() {
    if (prefs.getString(Preferences.langCode) == Preferences.japanese)
      _languageStore.changeLanguage(Preferences.english);
    else
      _languageStore.changeLanguage(Preferences.japanese);
  }
}
