import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/data/sharedpref/constants/preferences.dart';
import 'package:app_developer_assignment/generated/l10n.dart';
import 'package:app_developer_assignment/routing/routes.dart';
import 'package:app_developer_assignment/stores/language/language_store.dart';
import 'package:app_developer_assignment/ui/widgets/rounded_button_widget.dart';
import 'package:app_developer_assignment/ui/widgets/textfield_widget.dart';
import 'package:app_developer_assignment/utilities/utilities.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/service.dart';
import '../../utilities/utilities.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LanguageStore _languageStore;
  TextEditingController _userUsernameController = TextEditingController();
  SharedPreferences prefs;
  TextEditingController _userPasswordController = TextEditingController();
  ProgressDialog progressDialog = new ProgressDialog();
  final _formKey = GlobalKey<FormState>();
  bool validated = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // initializing stores
    _languageStore = Provider.of<LanguageStore>(context);
  }

  @override
  void initState() {
    super.initState();
    loadVariables();
  }

  loadVariables() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.white,
        elevation: 0,
        /*actions: [
          GestureDetector(
            onTap: () {
              changeLanguage();
            },
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                    height: 25,
                    width: 25,
                    color: AppColors.white,
                    image: AssetImage('assets/images/change_language.jpg')),
              ),
            ),
          )
        ],*/
      ),
      body: Container(
        color: AppColors.white,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Form(
          key: _formKey,
          onChanged: () =>
              setState(() => validated = _formKey.currentState.validate()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(child: Image(image: new AssetImage('assets/images/logo.png'),height: 100,)),
              Column(
                children: [
                  _buildUserEmailField(),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      bottom: 40,
                    ),
                    child: _buildPasswordField(),
                  ),
                  _buildLoginButton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildUserEmailField() {
    return TextFieldWidget(
      hint: S.of(context).enterUserMobile,
      maxLength: 50,
      marginLeft: 0,
      inputType: TextInputType.number,
      textController: _userUsernameController,
      inputAction: TextInputAction.next,
      icon: Icon(
        Icons.person,
        color: AppColors.white,
      ),
      validator: (value) {
        if (value == null || value.isEmpty || value.length < 3) {
          return S.of(context).usernameError;
        }
        return null;
      },
    );
  }

  Widget _buildPasswordField() {
    return TextFieldWidget(
      hint: S.of(context).enterPassword,
      isObscure: true,
      marginLeft: 0,
      inputType: TextInputType.text,
      textController: _userPasswordController,
      inputAction: TextInputAction.next,
      maxLength: 50,
      icon: Icon(Icons.security, color: AppColors.white),
      validator: (value) {
        if (value == null || value.isEmpty || value.length < 3) {
          return S.of(context).passwordError;
        }
        return null;
      },
    );
  }

  Widget _buildLoginButton() {
    return RoundedButtonWidget(
      buttonText: S.of(context).login,
      textColor: AppColors.white,
      buttonColor: AppColors.pink,
      onPressed: validated ? (() => callLoginAPI(context)) : null,
    );
  }

  callLoginAPI(context) async {


    Service apiSetup = Service();

    apiSetup.loginUser(
      mobile:_userUsernameController.text.trim(),
      password:_userPasswordController.text.trim(),
      success: (obj) {
        Navigator.pushReplacementNamed(context, Routes.home);
        //Navigator.push(context, MaterialPageRoute(builder: (_) => GiftCard()));
      },
      fail: (message) {
        Utilities.showToast(message, false);
      },
    );
  }
  changeLanguage() {
    if (prefs.getString(Preferences.langCode) == Preferences.japanese)
      _languageStore.changeLanguage(Preferences.english);
    else
      _languageStore.changeLanguage(Preferences.japanese);
  }
}
