import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleTextWidget extends StatelessWidget {
  final String text;
  final double size;
  final Color color;
  final TextAlign textAlign;
  final FontWeight fontWeight;
  final bool wrap;

  const SimpleTextWidget(
      {this.text,
      this.size,
      this.fontWeight,
      this.color,
      this.wrap = true,
      this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      overflow: wrap ? TextOverflow.visible : TextOverflow.ellipsis,
      textAlign: textAlign,
      style: Theme.of(context).textTheme.button.copyWith(
          color: color,
          fontFamily: 'WorkSans',
          fontSize: size,
          fontWeight: fontWeight),
    );
  }
}
