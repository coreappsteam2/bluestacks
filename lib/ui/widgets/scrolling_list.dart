import 'package:app_developer_assignment/dto/category_dto.dart';
import 'package:app_developer_assignment/dto/game_dto.dart';
import 'package:app_developer_assignment/dto/product_dto.dart';
import 'package:app_developer_assignment/dto/user_list_dto.dart';
import 'package:app_developer_assignment/podo/category.dart';
import 'package:app_developer_assignment/podo/game.dart';
import 'package:app_developer_assignment/podo/product.dart';
import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/services/service.dart';
import 'package:app_developer_assignment/ui/home/order_list.dart';
import 'package:app_developer_assignment/ui/home/product_list.dart';
import 'package:app_developer_assignment/ui/list_items/category_item.dart';
import 'package:app_developer_assignment/ui/list_items/customer_item.dart';
import 'package:app_developer_assignment/ui/list_items/order_item.dart';
import 'package:app_developer_assignment/ui/list_items/partner_item.dart';
import 'package:app_developer_assignment/ui/list_items/product_item.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:app_developer_assignment/utilities/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../constants/app_colors.dart';
import '../../dto/user_dto.dart';
import '../../generated/l10n.dart';
import 'textfield_widget.dart';

class InfiniteListWidget extends StatefulWidget {
  @override
  InfiniteListWidgetState createState() => InfiniteListWidgetState();
}

class InfiniteListWidgetState extends State<InfiniteListWidget> {
  TextEditingController searchTextController=new TextEditingController();
  final PagingController<int, User> _pagingController =
      PagingController(firstPageKey: 0);
  final PagingController<int, Category> _categoryPagingController =
  PagingController(firstPageKey: 0);
  final PagingController<int, Product> _productPagingController =
  PagingController(firstPageKey: 0);
  Service service = new Service();
  List<User> gameData = [];
  static int page = 0;
  static String type="";
  static String searchProduct="";
  static String user_id="";
  bool noData=false;
  @override
  void initState() {
    page=0;
    _categoryPagingController.addPageRequestListener((pageKey) {
      if(noData){
        return;
      }
      if(type=="category")
        _fetchCategory();

    });
    _productPagingController.addPageRequestListener((pageKey) {
      if(noData){
        return;
      }
      if(type=="product")
        _fetchProduct();
      if(type=="cart")
        _fetchCart();

    });
    _pagingController.addPageRequestListener((pageKey) {
      if(noData){
        return;
      }
      if(type=="partner")
        _fetchPartner();
      if(type=="customer")
        _fetchCustomer();
      if(type=="category")
        _fetchCategory();
      if(type=="order")
        _fetchOrder();

      if(type=="cart")
        _fetchOrder();
    });
  }

  @override
  Widget build(BuildContext context){
    /*if(type=="customer")
    if(type=="partner")
    return customerList();
    if(type=="product")
    if(type=="cart")
    if(type=="category")
    */
    if(type=="cart")
      return cartList();
    if(type=="partner")
      return partnerList();
    if(type=="customer")
    return customerList();
    if(type=="order")
      return orderList();
    if(type=="category")
      return categoryList();
    if(type=="product")
      return productList();
  }
  Widget _buildUserSearchField() {
    return TextFieldWidget(
      hint: S.of(context).search,
      maxLength: 100,
      marginLeft: 0,
      inputType: TextInputType.text,
      textController: searchTextController,
      inputAction: TextInputAction.next,
      onChanged: (value){
        _productPagingController.itemList.clear();
        page=0;
        _fetchProduct();
      },
      icon: Icon(
        Icons.person,
        color: AppColors.white,
      ),
      validator: (value) {

        return null;
      },
    );
  }
  Widget productList(){
    return Column(
      children: [
        Container(
            height: MediaQuery.of(context).size.height *0.08,
            child: _buildUserSearchField()),
        Container(
          height: MediaQuery.of(context).size.height *0.70,
          child: PagedListView<int, Product>(
            pagingController: _productPagingController,
            builderDelegate: PagedChildBuilderDelegate<Product>(
              itemBuilder: (context, item, index) => productCard(item,index),
            ),
          ),
        ),
      ],
    );
  }
  Widget cartList(){
    return Column(
      children: [

        Container(
          height: MediaQuery.of(context).size.height *0.70,
          child: PagedListView<int, Product>(
            pagingController: _productPagingController,
            builderDelegate: PagedChildBuilderDelegate<Product>(
              itemBuilder: (context, item, index) => productCard(item,index),
            ),
          ),
        ),
      ],
    );
  }

  Widget productCard(product,index){
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
              decoration: new BoxDecoration(
                color: AppColors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25)),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 15.0, bottom: 5.0, left: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      child: ClipRRect(

                        borderRadius: BorderRadius.all(Radius.circular(25)),
                        child: Image(image: new NetworkImage(product.image),height: 50,width: 50,),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Container(
                              width: MediaQuery.of(context).size.width * .5,
                              child: SimpleTextWidget(
                                text: product.title.toString().trim(),
                                fontWeight: FontWeight.bold,
                                color: AppColors.black,
                                size: 18,
                                textAlign: TextAlign.left,
                                wrap: false,
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Container(
                              child: SimpleTextWidget(
                                text: "Customer Price : ₹ "+product.mrp.toString(),
                                color: AppColors.black.withOpacity(0.7),
                                size: 15,
                                textAlign: TextAlign.left,
                                wrap: false,
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Container(

                              child: SimpleTextWidget(
                                text: "Your Margin : ₹ "+int.parse((double.parse((product.mrp*(product.margin/100)).toString()).toStringAsFixed(0))).toString(),
                                color: AppColors.green,
                                fontWeight: FontWeight.bold,
                                size: 16,
                                textAlign: TextAlign.left,
                                wrap: false,
                              )),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: (){
                        markFavorite(product,index);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          product.favorite==1?Icons.favorite:Icons.favorite_border,
                          color: AppColors.red,
                        ),
                      ),
                    )
                  ],
                ),
              )),
          Container(
            margin: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                GestureDetector(
                  onTap: (){
                    setState(() {
                      if(productListData[index].qty>0)
                      productListData[index].qty--;
                    });
                  },
                  child: Container(
                    child: Icon(Icons.remove_circle),
                  ),
                ),
                SimpleTextWidget(
                    text: "${product.qty}",
                    size: 20.0,
                    fontWeight: FontWeight.bold,
                    color: AppColors.black),
                GestureDetector(
                  onTap: (){
                    setState(() {
                      productListData[index].qty++;
                    });
                    addToCart(product, index);
                  },
                  child: Container(
                    child: Icon(Icons.add_circle),
                  ),
                )
              ],

            ),
          )
        ],
      ),
    );
  }
  Widget categoryList(){
    return PagedListView<int, Category>(
      pagingController: _categoryPagingController,
      builderDelegate: PagedChildBuilderDelegate<Category>(
        itemBuilder: (context, item, index) => CategoryListItemWidget(
          category: item,
        ),
      ),
    );
  }
  Widget orderList(){
    return PagedListView<int, User>(
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<User>(
        itemBuilder: (context, item, index) => OrderListItemWidget(
          user: item,
        ),
      ),
    );
  }
  Widget customerList(){
    return PagedListView<int, User>(
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<User>(
        itemBuilder: (context, item, index) => CustomerListItemWidget(
          user: item,
        ),
      ),
    );
  }
  Widget partnerList(){
    return PagedListView<int, User>(
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<User>(
        itemBuilder: (context, item, index) => PartnerListItemWidget(
          user: item,
        ),
      ),
    );
  }

  Future<void> _fetchPartner() async {
    try {
      service.listPartner(
          page: page++,
          success: (UserListDTO data) {
            if(data.users.length==0){
              noData=true;
              _pagingController.appendLastPage(data.users);
            }else{
              _pagingController.appendPage(data.users, 0);
            }


          },
          fail: () {});
      /*final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = cursor + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }*/
    } catch (error) {
      _pagingController.error = error;
    }
  }
  Future<void> _fetchCustomer() async {
    try {
      service.listCustomer(
          page: page++,
          success: (UserListDTO data) {
            if(data.users.length==0){
              noData=true;
              _pagingController.appendLastPage(data.users);
            }else {
              _pagingController.appendPage(data.users, 0);
            }
          },
          fail: () {});
      /*final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = cursor + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }*/
    } catch (error) {
      _pagingController.error = error;
    }
  }
  Future<void> _fetchCategory() async {
    try {
      service.listCategory(
        page:page++,
          success: (CategoryDTO data) {
            if(data.categories.length==0){
              noData=true;
              _categoryPagingController.appendLastPage(data.categories);
            }else{
              _categoryPagingController.appendPage(data.categories, 0);
            }

          },
          fail: () {});
      /*final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = cursor + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }*/
    } catch (error) {
      _pagingController.error = error;
    }
  }
  Future<void> _fetchOrder() async {
    try {
      service.listOrder(
        user_id: OrderListState.user_id,
        page: page++,
          success: (UserListDTO data) {
            if(data.users.length==0){
              noData=true;
              _pagingController.appendLastPage(data.users);
            }else{
              _pagingController.appendPage(data.users, 0);
            }

          },
          fail: () {});
      /*final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = cursor + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }*/
    } catch (error) {
      _pagingController.error = error;
    }
  }
  List<Product> productListData=new List<Product>();
  Future<void> _fetchProduct() async {
    try {
      service.listProduct(
        page: page++,
          keyword:searchTextController.text.trim().toString(),
          category_id:ProductListState.category_id,
          success: (ProductListDTO data) {
            if(data.products.length==0){
              noData=true;
              _productPagingController.appendLastPage(data.products);

            }else{
              _productPagingController.appendPage(data.products, 0);

            }
            productListData.addAll(data.products);

          },
          fail: () {});
      /*final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = cursor + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }*/
    } catch (error) {
      _pagingController.error = error;
    }
  }
  Future<void> _fetchCart() async {
    try {
      service.listCart(
          page: page++,
          user_id: OrderListState.user_id,
          success: (ProductListDTO data) {
            if(data.products.length==0 ){
              noData=true;
              _productPagingController.appendLastPage(data.products);
            }else{
              _productPagingController.appendPage(data.products, 0);
            }

          },
          fail: () {});
      /*final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = cursor + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }*/
    } catch (error) {
      _pagingController.error = error;
    }
  }
  markFavorite(Product product,index){

    Service apiSetup = Service();

    apiSetup.favoriteProduct(
      product_id:product.id.toString(),
      type:"1",
      success: (obj) {
        setState(() {
          productListData[index].favorite=(productListData[index].favorite==0?1:0);
        });

      },
      fail: (message) {
        Utilities.showToast(message, false);
      },
    );
  }
  addToCart(Product product,index){

    Service apiSetup = Service();

    apiSetup.addToCart(
      user_id:OrderListState.user_id,
      product_id:product.id.toString(),
      category_id:ProductListState.category_id.toString(),
      qty:product.qty.toString(),
      amount:(product.mrp * product.qty).toString(),
      total_amount:(product.mrp * product.qty).toString(),
      margin:product.margin.toString(),
      mrp:product.mrp.toString(),
      success: (obj) {
        setState(() {

        });

      },
      fail: (message) {
        Utilities.showToast(message, false);
      },
    );
  }
}
