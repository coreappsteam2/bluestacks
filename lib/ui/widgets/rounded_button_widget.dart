import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:flutter/material.dart';

class RoundedButtonWidget extends StatelessWidget {
  final String buttonText;
  final Color buttonColor;
  final Color textColor;
  final VoidCallback onPressed;
  final int spaceVal;

  RoundedButtonWidget(
      {Key key,
      this.buttonText,
      this.buttonColor,
      this.textColor = Colors.white,
      this.onPressed,
      this.spaceVal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: MediaQuery.of(context).size.width - 40,
        height: 50,
        child: FlatButton(
            color: buttonColor,
            disabledColor: AppColors.pink.withOpacity(0.3),
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: Colors.transparent,
                    width: 1,
                    style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(20)),
            onPressed: onPressed,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  buttonText,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.button.copyWith(
                      color: textColor,
                      fontFamily: 'WorkSans',
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                )
              ],
            )),
      ),
    );
  }
}
