import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final Widget icon;
  final String hint;
  final String errorText;
  final bool isObscure;
  final bool isIcon;
  final TextInputType inputType;
  final TextEditingController textController;
  final EdgeInsets padding;
  final Color hintColor;
  final double marginLeft;
  final int maxLength;
  final Color iconColor;
  final FocusNode focusNode;
  final ValueChanged onFieldSubmitted;
  final ValueChanged onChanged;
  final bool autoFocus;
  final TextInputAction inputAction;
  final String label;
  final FormFieldValidator validator;

  // final Widget icon;
  final Widget suffix; // this is added

  const TextFieldWidget(
      {Key key,
      this.icon,
      this.hint,
      this.errorText,
      this.isObscure = false,
      this.inputType,
      this.textController,
      this.isIcon = true,
      this.padding = const EdgeInsets.all(0),
      this.marginLeft = 0.0,
      this.hintColor = Colors.grey,
      this.iconColor = Colors.grey,
      this.focusNode,
      this.onFieldSubmitted,
      this.onChanged,
      this.autoFocus = true,
      this.inputAction,
      this.label,
      this.suffix,
      this.maxLength,
      this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: marginLeft, right: 0),
      child: Padding(
        padding: padding,
        child: TextFormField(
          controller: textController,
          focusNode: focusNode,
          onFieldSubmitted: onFieldSubmitted,
          onChanged: onChanged,
          validator: validator,
          autofocus: autoFocus,
          textInputAction: inputAction,
          obscureText: this.isObscure,
          maxLength: maxLength,
          keyboardType: this.inputType,
          style: new TextStyle(
              color: AppColors.black, fontSize: 16, fontFamily: 'WorkSans'),
          decoration: InputDecoration(
            suffix: suffix,
            prefixIcon: icon,

            // this is added
            labelText: this.label,
            focusedErrorBorder:  new OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(23)),
                borderSide: new BorderSide(
                    width: 2.0,
                    color: AppColors.red,
                    style: BorderStyle.solid)),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(23)),
              borderSide: BorderSide(
                  width: 2.0, color: AppColors.grey, style: BorderStyle.solid),
            ),
            enabledBorder: new OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(23)),
                borderSide: new BorderSide(
                    width: 2.0,
                    color: AppColors.grey,
                    style: BorderStyle.solid)),
            hintText: this.hint,
            errorBorder:new OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(23)),
                borderSide: new BorderSide(
                    width: 2.0,
                    color: AppColors.red,
                    style: BorderStyle.solid)) ,
            hintStyle: new TextStyle(
                color: AppColors.grey, fontSize: 14, fontFamily: 'WorkSans'),
            errorText: errorText,
            counterText: '',
          ),
        ),
      ),
    );
  }
}
