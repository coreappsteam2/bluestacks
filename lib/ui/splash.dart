import 'dart:async';

import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/data/sharedpref/constants/preferences.dart';
import 'package:app_developer_assignment/routing/routes.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/service.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with WidgetsBindingObserver {
  SharedPreferences preference;

  _loadPref() async {
    preference = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    super.initState();
    _loadPref();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Container(
        color: AppColors.pink,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Image.asset('assets/images/logo.png', height: 100),
            ),
          ],
        ),
      ),
    ));
  }

  startTimer() {
    var _duration = Duration(milliseconds: 5000);
    return Timer(_duration, navigate);
  }

  navigate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Service service=new Service();
    service.loadSharedPrefs();
    //preferences.clear();
    if (preferences.getBool(Preferences.isLoggedIn) != null ||
        preferences.getBool(Preferences.isLoggedIn) == true) {
      Navigator.of(context).pushReplacementNamed(Routes.home);
    } else {
      Navigator.of(context).pushReplacementNamed(Routes.login);
    }
  }
}
