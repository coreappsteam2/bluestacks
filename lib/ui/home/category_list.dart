import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/data/sharedpref/constants/preferences.dart';
import 'package:app_developer_assignment/dto/user_dto.dart';
import 'package:app_developer_assignment/generated/l10n.dart';
import 'package:app_developer_assignment/podo/user_singleton.dart';
import 'package:app_developer_assignment/routing/routes.dart';
import 'package:app_developer_assignment/services/service.dart';
import 'package:app_developer_assignment/stores/language/language_store.dart';
import 'package:app_developer_assignment/ui/widgets/scrolling_list.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:app_developer_assignment/utilities/utilities.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants/app_colors.dart';

class CategoryList extends StatefulWidget {
  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  ProgressDialog progressDialog = new ProgressDialog();
  SharedPreferences prefs;
  Service service = new Service();
  LanguageStore _languageStore;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // initializing stores
    _languageStore = Provider.of<LanguageStore>(context);
  }

  @override
  void initState() {
    super.initState();
    loadVariables();

  }

  loadVariables() async {
    prefs = await SharedPreferences.getInstance();
  }

  changeLanguage() {
    if (prefs.getString(Preferences.langCode) == Preferences.japanese)
      _languageStore.changeLanguage(Preferences.english);
    else
      _languageStore.changeLanguage(Preferences.japanese);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.pink,
        elevation: 0,

        title: SimpleTextWidget(
            text: S.of(context).categories,
            size: 18.0,
            fontWeight: FontWeight.bold,
            color: AppColors.white),
        /*actions: [
          GestureDetector(
            onTap: () {
              changeLanguage();
            },
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                    height: 25,
                    width: 25,
                    image: AssetImage('assets/images/change_language.jpg')),
              ),
            ),
          )
        ],*/
        centerTitle: true,
      ),

      body: Container(
        color: AppColors.white,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child:buildScrollListWidget()
        ,
      ),
    );
  }

  Widget buildScrollListWidget() {
    InfiniteListWidgetState.type="category";
    return Container(
        height: MediaQuery.of(context).size.height *0.90,
        child: InfiniteListWidget());
  }

}
