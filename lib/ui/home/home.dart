import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:app_developer_assignment/data/sharedpref/constants/preferences.dart';
import 'package:app_developer_assignment/dto/dashboard_dto.dart';
import 'package:app_developer_assignment/dto/user_dto.dart';
import 'package:app_developer_assignment/generated/l10n.dart';
import 'package:app_developer_assignment/podo/user_singleton.dart';
import 'package:app_developer_assignment/routing/routes.dart';
import 'package:app_developer_assignment/services/service.dart';
import 'package:app_developer_assignment/stores/language/language_store.dart';
import 'package:app_developer_assignment/ui/widgets/scrolling_list.dart';
import 'package:app_developer_assignment/ui/widgets/simple_text_widget.dart';
import 'package:app_developer_assignment/utilities/utilities.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:need_resume/need_resume.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants/app_colors.dart';
import '../widgets/scrolling_list.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends ResumableState<HomeScreen>  {
  ProgressDialog progressDialog = new ProgressDialog();
  SharedPreferences prefs;
  Service service = new Service();
  LanguageStore _languageStore;
  DashboardDTO dashboardDTO;
  @override
  void onResume() {
    // Implement your code inside here
    Service service=new Service();
    service.loadSharedPrefs();
    //getCall();
    print('HomeScreen is resumed!');
    setState(() {

    });
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // initializing stores
    _languageStore = Provider.of<LanguageStore>(context);
  }

  @override
  void initState() {
    super.initState();
    loadVariables();
    Service service=new Service();
    service.loadSharedPrefs();
    getUserInfo();

  }

  getUserInfo() {
    service.getUserData(success: (DashboardDTO dashboardDTO) {
      setState(() {
        if(dashboardDTO.dashboard.name!=null) {
          this.dashboardDTO = dashboardDTO;
          UserSingleton.instance.referral_code =
              this.dashboardDTO.dashboard.referral_code;
        }
      });
    }, fail: (UserDTO userDTO) {
      Utilities.showToast(userDTO.replyMsg, false);
    });
  }

  loadVariables() async {
    prefs = await SharedPreferences.getInstance();
  }

  changeLanguage() {
    if (prefs.getString(Preferences.langCode) == Preferences.japanese)
      _languageStore.changeLanguage(Preferences.english);
    else
      _languageStore.changeLanguage(Preferences.japanese);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.pink,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            prefs.clear();
            Navigator.pushReplacementNamed(context, Routes.login);
          },
          child: Icon(
            Icons.menu,
            color: AppColors.white,
          ),
        ),
        title: SimpleTextWidget(
            text: S.of(context).flyingWolf,
            size: 18.0,
            fontWeight: FontWeight.bold,
            color: AppColors.white),
        /*actions: [
          GestureDetector(
            onTap: () {
              changeLanguage();
            },
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image(
                    height: 25,
                    width: 25,
                    image: AssetImage('assets/images/change_language.jpg')),
              ),
            ),
          )
        ],*/
        centerTitle: true,
      ),
      floatingActionButton:FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: AppColors.pink,
        onPressed: (){
          //TODO
          if(Service.mode=="partner") {
            Navigator.pushNamed(context, Routes.addCustomer).then((value){
            setState(() {

            });
          });
          }else {
            Navigator.pushNamed(context, Routes.addPartner).then((value) {
              setState(() {

              });
            });
          }
        },
      ),
      body: Container(
        color: AppColors.white,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: UserSingleton.instance.name != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  buildUserProfileWidget(),
                  dashboardDTO!=null?buildStatisticsWidget():Container(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 15),
                        child: SimpleTextWidget(
                            text: S.of(context).yourPartners,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.black),
                      ),
                      GestureDetector(
                        onTap: (){
                          if(Service.mode=="partner")
                            Navigator.pushNamed(context, Routes.customerList).then((value){
                              setState(() {

                              });
                            });
                          else
                          Navigator.pushNamed(context, Routes.partnerList).then((value){
                            setState(() {

                            });
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 15),
                          child: SimpleTextWidget(
                              text: S.of(context).viewAll,
                              size: 20.0,
                              fontWeight: FontWeight.bold,
                              color: AppColors.black),
                        ),
                      ),
                    ],
                  ),
                  buildScrollListWidget()
                ],
              )
            : Container(),
      ),
    );
  }

  Widget buildScrollListWidget() {
    InfiniteListWidgetState.type="customer";
    return Container(
        height: MediaQuery.of(context).size.height * 0.18,
        child: InfiniteListWidget());
  }

  Widget buildStatisticsWidget() {
    return Container(
      height: 320,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 135,
                  decoration: new BoxDecoration(
                    color: AppColors.orange,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        bottomLeft: Radius.circular(25)),
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SimpleTextWidget(
                            text:
                                "${dashboardDTO.dashboard.pending_sales.toString()}",
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.white),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleTextWidget(
                            text: S.of(context).pendingSales,
                            size: 16.0,
                            fontWeight: FontWeight.normal,
                            color: AppColors.white),
                      ],
                    ),
                  )),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 1, vertical: 10),
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 135,
                  decoration: new BoxDecoration(
                    color: AppColors.violet,
                    shape: BoxShape.rectangle,
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SimpleTextWidget(
                            text:
                            "${dashboardDTO.dashboard.total_agents_added.toString()}",
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.white),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleTextWidget(
                            text: S.of(context).agentsAdded,
                            size: 16.0,
                            fontWeight: FontWeight.normal,
                            color: AppColors.white),
                      ],
                    ),
                  )),
              Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 135,
                  decoration: new BoxDecoration(
                    color: AppColors.crimson,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(25),
                        bottomRight: Radius.circular(25)),
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SimpleTextWidget(
                            text:
                            "${dashboardDTO.dashboard.total_earnings.toString()}",
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.white),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleTextWidget(
                            text: S.of(context).totalEarnings,
                            size: 16.0,
                            fontWeight: FontWeight.normal,
                            color: AppColors.white),
                      ],
                    ),
                  )),
            ],
          ),
          Row(
            children: [
              Container(
                  margin: EdgeInsets.symmetric(vertical: 2),
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 135,
                  decoration: new BoxDecoration(
                    color: AppColors.orange,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        bottomLeft: Radius.circular(25)),
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SimpleTextWidget(
                            text:
                            "${dashboardDTO.dashboard.total_sales.toString()}",
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.white),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleTextWidget(
                            text: S.of(context).totalSales,
                            size: 16.0,
                            fontWeight: FontWeight.normal,
                            color: AppColors.white),
                      ],
                    ),
                  )),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 1, vertical: 10),
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 135,
                  decoration: new BoxDecoration(
                    color: AppColors.violet,
                    shape: BoxShape.rectangle,
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SimpleTextWidget(
                            text:
                            "${dashboardDTO.dashboard.total_orders.toString()}",
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.white),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleTextWidget(
                            text: S.of(context).totalOrders,
                            size: 16.0,
                            fontWeight: FontWeight.normal,
                            color: AppColors.white),
                      ],
                    ),
                  )),
              Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 135,
                  decoration: new BoxDecoration(
                    color: AppColors.crimson,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(25),
                        bottomRight: Radius.circular(25)),
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SimpleTextWidget(
                            text:
                            "${dashboardDTO.dashboard.total_products.toString()}",
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                            color: AppColors.white),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleTextWidget(
                            text: S.of(context).totalProducts,
                            size: 16.0,
                            fontWeight: FontWeight.normal,
                            color: AppColors.white),
                      ],
                    ),
                  )),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildUserProfileWidget() {
    return Container(
      height: 90,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
              width: 70.0,
              height: 70.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(50.0),
                  image: new DecorationImage(
                      image: new NetworkImage(UserSingleton.instance.image??"")))),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SimpleTextWidget(
                    text: "${UserSingleton.instance.name}",
                    size: 16.0,
                    fontWeight: FontWeight.bold,
                    color: AppColors.black),
              ),
              Container(
                  height: 40.0,
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border.all(
                      color: AppColors.blue,
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  child: GestureDetector(
                    onTap: (){

                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left:8.0,right: 4.0),
                          child: SimpleTextWidget(
                              text: S.of(context).referralCode,
                              size: 16.0,
                              fontWeight: FontWeight.normal,
                              color: AppColors.black),
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 2.0, right: 1.0),
                            child: SimpleTextWidget(
                                text:
                                    "${UserSingleton.instance.referral_code.toString()}",
                                size: 20.0,
                                fontWeight: FontWeight.bold,
                                color: AppColors.blue),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Icon(Icons.share,color: AppColors.pink,size: 15,),
                        )

                      ],
                    ),
                  )),
            ],
          )
        ],
      ),
    );
  }
}
