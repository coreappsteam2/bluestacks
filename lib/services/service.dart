import 'dart:convert' as convert;

import 'package:app_developer_assignment/data/sharedpref/constants/preferences.dart';
import 'package:app_developer_assignment/dto/category_dto.dart';
import 'package:app_developer_assignment/dto/dashboard_dto.dart';
import 'package:app_developer_assignment/dto/game_dto.dart';
import 'package:app_developer_assignment/dto/product_dto.dart';
import 'package:app_developer_assignment/dto/user_dto.dart';
import 'package:app_developer_assignment/dto/user_list_dto.dart';
import 'package:app_developer_assignment/utilities/utilities.dart';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../data/sharedpref/constants/preferences.dart';
import '../data/sharedpref/constants/preferences.dart';
import '../data/sharedpref/constants/preferences.dart';
import '../dto/base_dto.dart';
import '../dto/base_dto.dart';
import '../podo/user_singleton.dart';
import '../podo/user_singleton.dart';

class Service {
  static String version = "v0.0.2";
  static String mode="partner";//agent
  static String apiBase="http://app.petopian.in:8789/";
  static String login=apiBase+"login";
  static String register_user=apiBase+"register_user";
  static String dashboard=apiBase+"agent_dashboard";
  static String list_partner=apiBase+"list_partner";
  static String list_customer=apiBase+"list_customer";
  static String list_category=apiBase+"app_category_list";
  static String list_order=apiBase+"user_order_list";
  static String list_product=apiBase+"product_list";
  static String list_cart=apiBase+"temp_cart_list";
  static String add_to_cart=apiBase+"add_to_cart";
  static String mark_product_fav=apiBase+"mark_product_fav";
  static String tournamentGet =
      'http://tournaments-dot-game-tv-prod.uc.r.appspot.com/tournament/api/tournaments_list_v2?limit=10&status=all';
  static String userData = 'https://coreappstech.com/user.json';
  static String imageBase = "http://18.220.93.38/petopian/uploads/";
  SharedPreferences prefs;

  //---------------------------------------------------------//
  loginUser({mobile,password,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    UserDTO userDTO;
    Map data = {
      "phone": mobile,
      "password": password,
      "device_id": "2121212",
      //"device_token": prefs.getString(Preferences.device_token)
      ///"device_type":Platform.operatingSystem
    };
    /*var token = await prefs.getString(Preferences.auth_token);
    Map<String, String> header = new Map();
    header["Token"] = token;*/
    var response =
    await http.post("$login", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    userDTO = UserDTO.jsonResponse(body);
    if(userDTO.replyCode=="success") {
      prefs.setBool(Preferences.isLoggedIn, true);
      prefs.setString(Preferences.sid, body['sid']);
      prefs.setString(
          Preferences.referral_code, UserSingleton.instance.referral_code);
      prefs.setString(
          Preferences.image, UserSingleton.instance.image);
      prefs.setString(
          Preferences.username, UserSingleton.instance.name);
      prefs.setString(
          Preferences.mobile, UserSingleton.instance.mobile);
      prefs.setString(
          Preferences.partner_id, UserSingleton.instance.partner_id.toString());
      prefs.setString(
          Preferences.agent_id, UserSingleton.instance.agent_id.toString());
      prefs.setString(
          Preferences.id, UserSingleton.instance.id.toString());
      success(userDTO);
    }else{
      fail(userDTO.replyMsg);
    }

  }

  favoriteProduct({product_id,type,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    BaseDTO baseDTO;
    Map data = {
      "product_id": product_id,
      "type": type,
      "sid": prefs.getString(Preferences.sid),

    };

    var response =
    await http.post("$mark_product_fav", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    baseDTO = BaseDTO.jsonResponse(body);

    if (baseDTO.replyCode == "success") {
      success(baseDTO);
    } else {
      fail(baseDTO.replyMsg);
    }
  }
  createPartner({mobile,email,fullName,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    BaseDTO baseDTO;
    var agent_id= prefs.getString(Preferences.id);
    var username= prefs.getString(Preferences.username);
    Map data = {
      "phone": mobile,
      "email": email,
      "fullName": fullName,
      "partner_id": "",
      "agent_id": prefs.getString(Preferences.id),
      "partner_name": prefs.getString(Preferences.username),

    };

    var response =
    await http.post("$register_user", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    baseDTO = BaseDTO.jsonResponse(body);

    if (baseDTO.replyCode == "success") {
      success(baseDTO);
    } else {
      fail(baseDTO.replyMsg);
    }
  }
  createCustomer({mobile,email,fullName,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    BaseDTO baseDTO;
    Map data = {
      "phone": mobile,
      "email": email,
      "fullName": fullName,
      "partner_id": prefs.getString(Preferences.id),
      "agent_id": "",
      "sponsor_name": prefs.getString(Preferences.username),

    };

    var response =
    await http.post("$register_user", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    baseDTO = BaseDTO.jsonResponse(body);

    if (baseDTO.replyCode == "success") {
      success(baseDTO);
    } else {
      fail(baseDTO.replyMsg);
    }
  }
  listCustomer({page,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    UserListDTO userDTO;
    Map data = {
      "page":page.toString(),
      "partner_id": prefs.getString(Preferences.id)
    };

    var response =
    await http.post("$list_customer", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    userDTO = UserListDTO.jsonResponse(body);

    if (userDTO.replyCode == "success") {
      success(userDTO);
    } else {
      fail(userDTO.replyMsg);
    }
  }
  listCategory({page,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    CategoryDTO categoryDTO;
    Map data = {
      "page":page.toString()
    };

    var response =
    await http.post("$list_category", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    categoryDTO = CategoryDTO.jsonResponse(body);

    if (categoryDTO.replyCode == "success") {
      success(categoryDTO);
    } else {
      fail(categoryDTO.replyMsg);
    }
  }
  listOrder({page,user_id,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    UserListDTO userDTO;
    Map data = {
      "page":page.toString(),
      "user_id":user_id
    };

    var response =
    await http.post("$list_order", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    userDTO = UserListDTO.jsonResponse(body);

    if (userDTO.replyCode == "success") {
      success(userDTO);
    } else {
      fail(userDTO.replyMsg);
    }
  }
  listProduct({page,category_id,keyword,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    ProductListDTO productListDTO;
    Map data = {
      "page":page.toString(),
      "category_id":category_id,
      "sid":prefs.getString(Preferences.sid),
      "keyword":keyword
    };

    var response =
    await http.post("$list_product", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    productListDTO = ProductListDTO.jsonResponse(body);

    if (productListDTO.replyCode == "success") {
      success(productListDTO);
    } else {
      fail(productListDTO.replyMsg);
    }
  }
  listCart({user_id,page,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    ProductListDTO productDTO;
    Map data = {
      "page":page.toString(),
      "user_id":user_id,
    };

    var response =
    await http.post("$list_cart", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    productDTO = ProductListDTO.jsonResponse(body);

    if (productDTO.replyCode == "success") {
      success(productDTO);
    } else {
      fail(productDTO.replyMsg);
    }
  }
  addToCart({user_id,product_id,category_id,qty,amount,page,total_amount,margin,mrp,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    BaseDTO userDTO;
    Map data = {
      "user_id":user_id,
      "product_id":product_id,
      "category_id":category_id,
      "amount":amount,
      "qty":qty,
      "total_amount":total_amount,
      "margin":margin,
      "mrp":mrp,
      "reseller_id":prefs.getString(Preferences.id),
    };

    var response =
    await http.post("$add_to_cart", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    userDTO = BaseDTO.jsonResponse(body);

    if (userDTO.replyCode == "success") {
      success(userDTO);
    } else {
      fail(userDTO.replyMsg);
    }
  }

  listPartner({page,Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    UserListDTO userDTO;
    Map data = {
      "page":page.toString(),
      "agent_id": prefs.getString(Preferences.id)
    };

    var response =
    await http.post("$list_partner", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);

    userDTO = UserListDTO.jsonResponse(body);

    if (userDTO.replyCode == "success") {
      success(userDTO);
    } else {
      fail(userDTO.replyMsg);
    }
  }

  getUserData({Function success, Function fail}) async {
    prefs = await SharedPreferences.getInstance();
    bool connection = await checkInternet();
    if (!connection) return;
    DashboardDTO dashboardDTO;
    Map data = {
      "user_id": prefs.getString(Preferences.id)
    };

    var response =
    await http.post("$dashboard", headers: {}, body: data);

    final body = convert.jsonDecode(response.body);
    if(body['data'].length>0) {
      dashboardDTO = DashboardDTO.jsonResponse(body);
    }else{
      fail("No statistics found");
    }
    if (dashboardDTO.replyCode == "success") {
      success(dashboardDTO);
    } else {
      fail(dashboardDTO.replyMsg);
    }
  }
  void loadSharedPrefs() async{
    prefs = await SharedPreferences.getInstance();
    UserSingleton.instance.name=prefs.getString(Preferences.username);
    UserSingleton.instance.id=int.parse(prefs.getString(Preferences.id));
    UserSingleton.instance.mobile=prefs.getString(Preferences.mobile);
    UserSingleton.instance.image=prefs.getString(Preferences.image);
    UserSingleton.instance.referral_code=prefs.getString(Preferences.referral_code);
  }
  Future<bool> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      Utilities utility = Utilities();
      utility.showToastMessage(
          'It seems you are not connected to internet. Please try again.');
      return false;
    }
  }
}
