import 'package:app_developer_assignment/constants/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Utilities {
  static String _langCode = "en";

  void showToastMessage(String message) {
    Fluttertoast.showToast(msg: message);
  }

  static bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  static void showToast(String message, bool success) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: success ? AppColors.white : Colors.red,
        textColor: AppColors.white,
        fontSize: 16.0);
  }

  static void startAnimation(progressDialog, context, message) {
    progressDialog.showProgressDialog(context,
        textToBeDisplayed: message, dismissAfter: const Duration(seconds: 10));
  }

  static void stopAnimation(progressDialog, context) {
    progressDialog.dismissProgressDialog(context);
  }

  static List<String> validUsernames = ['9898989898', '9876543210'];
  static List<String> validPasswords = ['password123'];

  static String getLanguageCode() {
    return _langCode;
  }

  static changeLanguage(langCode) {
    _langCode = langCode;
  }
}
