import 'package:app_developer_assignment/podo/game.dart';

import 'base_dto.dart';

class GameDTO extends BaseDTO {
  List<Game> gameData = [];

  GameDTO.jsonResponse(Map json) : super.jsonResponse(json) {
    var data = json['data']['tournaments'];
    for (int i = 0; i < data.length; i++) {
      gameData.add(Game(data[i]));
    }
  }
}
