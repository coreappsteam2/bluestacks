import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/podo/user_singleton.dart';

import 'base_dto.dart';

class UserListDTO extends BaseDTO {
  List<User> users;
  UserListDTO.jsonResponse(Map json) : super.jsonResponse(json) {
    users=[];
    for(int i=0;i<json['data'].length;i++){
      users.add(User.parseJson(json['data'][i]));
    }

  }
}
