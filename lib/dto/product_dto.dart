import 'package:app_developer_assignment/podo/product.dart';

import 'base_dto.dart';

class ProductListDTO extends BaseDTO {
  List<Product> products;
  ProductListDTO.jsonResponse(Map json) : super.jsonResponse(json) {
    products=[];
    for(int i=0;i<json['data'].length;i++){
      products.add(Product.parseJson(json['data'][i]));
    }

  }
}
