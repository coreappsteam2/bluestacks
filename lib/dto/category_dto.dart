import 'package:app_developer_assignment/podo/category.dart';
import 'package:app_developer_assignment/podo/user.dart';
import 'package:app_developer_assignment/podo/user_singleton.dart';

import 'base_dto.dart';

class CategoryDTO extends BaseDTO {
  List<Category> categories;
  CategoryDTO.jsonResponse(Map json) : super.jsonResponse(json) {
    categories=[];
    for(int i=0;i<json['data'].length;i++){
      categories.add(Category.parseJson(json['data'][i]));
    }

  }
}
