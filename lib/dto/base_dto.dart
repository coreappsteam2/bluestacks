class BaseDTO {
  String message;
  bool error;
  String replyMsg;
  String replyCode;
  String cursor;

  BaseDTO.jsonResponse(Map json) {
    this.error = json['error'];
    this.replyMsg = json['replyMsg'];
    this.replyCode = json['replyCode'];
//    this.cursor = json['data']['cursor'];
  }
}
