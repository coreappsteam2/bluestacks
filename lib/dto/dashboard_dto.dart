import 'package:app_developer_assignment/podo/dashboard.dart';
import 'package:app_developer_assignment/podo/user_singleton.dart';

import 'base_dto.dart';

class DashboardDTO extends BaseDTO {
  Dashboard dashboard;
  DashboardDTO.jsonResponse(Map json) : super.jsonResponse(json) {
    dashboard=Dashboard.parseJson(json['data'][0]);
  }
}
