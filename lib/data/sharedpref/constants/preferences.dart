class Preferences {
  Preferences._();

  static const String isLoggedIn = 'isLoggedIn';
  static const String username = 'username';
  static const String langCode = 'langCode';
  static const String japanese = 'ja';
  static const String english = 'en';
  static const String device_token = 'device_token';
  static const String mobile = 'mobile';
  static const String auth_token = 'auth_token';
  static const String referral_code = 'referral_code';
  static const String image = 'image';
  static const String sid = 'sid';
  static const String id = 'id';
  static const String partner_id = 'partner_id';
  static const String agent_id = 'agent_id';

}
