// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Dashboard`
  String get flyingWolf {
    return Intl.message(
      'Dashboard',
      name: 'flyingWolf',
      desc: '',
      args: [],
    );
  }

  /// `Tournaments \nPlayed`
  String get tournamentsPlayed {
    return Intl.message(
      'Tournaments \nPlayed',
      name: 'tournamentsPlayed',
      desc: '',
      args: [],
    );
  }

  /// `Tournaments \nWon`
  String get tournamentsWon {
    return Intl.message(
      'Tournaments \nWon',
      name: 'tournamentsWon',
      desc: '',
      args: [],
    );
  }

  /// `Winning \nPercentage`
  String get winningPercentage {
    return Intl.message(
      'Winning \nPercentage',
      name: 'winningPercentage',
      desc: '',
      args: [],
    );
  }

  /// `Your Partners`
  String get yourPartners {
    return Intl.message(
      'Your Partners',
      name: 'yourPartners',
      desc: '',
      args: [],
    );
  }

  /// `View All`
  String get viewAll {
    return Intl.message(
      'View All',
      name: 'viewAll',
      desc: '',
      args: [],
    );
  }

  /// `Partners joined`
  String get partnersAdded {
    return Intl.message(
      'Partners joined',
      name: 'partnersAdded',
      desc: '',
      args: [],
    );
  }

  /// `Agents Joined`
  String get agentsAdded {
    return Intl.message(
      'Agents Joined',
      name: 'agentsAdded',
      desc: '',
      args: [],
    );
  }

  /// `Total Sales`
  String get totalSales {
    return Intl.message(
      'Total Sales',
      name: 'totalSales',
      desc: '',
      args: [],
    );
  }

  /// `Total Orders`
  String get totalOrders {
    return Intl.message(
      'Total Orders',
      name: 'totalOrders',
      desc: '',
      args: [],
    );
  }

  /// `Total Products`
  String get totalProducts {
    return Intl.message(
      'Total Products',
      name: 'totalProducts',
      desc: '',
      args: [],
    );
  }

  /// `Pending Sales`
  String get pendingSales {
    return Intl.message(
      'Pending Sales',
      name: 'pendingSales',
      desc: '',
      args: [],
    );
  }

  /// `Total Earnings`
  String get totalEarnings {
    return Intl.message(
      'Total Earnings',
      name: 'totalEarnings',
      desc: '',
      args: [],
    );
  }

  /// `Referral Code`
  String get referralCode {
    return Intl.message(
      'Referral Code',
      name: 'referralCode',
      desc: '',
      args: [],
    );
  }

  /// `Loading Users`
  String get loadingUsers {
    return Intl.message(
      'Loading Users',
      name: 'loadingUsers',
      desc: '',
      args: [],
    );
  }

  /// `Enter Name`
  String get enterUserFullname {
    return Intl.message(
      'Enter Name',
      name: 'enterUserFullname',
      desc: '',
      args: [],
    );
  }

  /// `Enter Mobile Number`
  String get enterUserMobile {
    return Intl.message(
      'Enter Mobile Number',
      name: 'enterUserMobile',
      desc: '',
      args: [],
    );
  }

  /// `Enter Email ID`
  String get enterUserEmail {
    return Intl.message(
      'Enter Email ID',
      name: 'enterUserEmail',
      desc: '',
      args: [],
    );
  }

  /// `Enter Username`
  String get enterUsername {
    return Intl.message(
      'Enter Username',
      name: 'enterUsername',
      desc: '',
      args: [],
    );
  }

  /// `Name should be more then 3 characters`
  String get userfullnameError {
    return Intl.message(
      'Name should be more then 3 characters',
      name: 'userfullnameError',
      desc: '',
      args: [],
    );
  }

  /// `Please enter valid mobile number`
  String get usernameError {
    return Intl.message(
      'Please enter valid mobile number',
      name: 'usernameError',
      desc: '',
      args: [],
    );
  }

  /// `Mobile Number should be 10 digits`
  String get userMobileError {
    return Intl.message(
      'Mobile Number should be 10 digits',
      name: 'userMobileError',
      desc: '',
      args: [],
    );
  }

  /// `Enter Password`
  String get enterPassword {
    return Intl.message(
      'Enter Password',
      name: 'enterPassword',
      desc: '',
      args: [],
    );
  }

  /// `Password should be more then 3 characters`
  String get passwordError {
    return Intl.message(
      'Password should be more then 3 characters',
      name: 'passwordError',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Add partner`
  String get addPartner {
    return Intl.message(
      'Add partner',
      name: 'addPartner',
      desc: '',
      args: [],
    );
  }

  /// `Add customer`
  String get addCustomer {
    return Intl.message(
      'Add customer',
      name: 'addCustomer',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Username/Password`
  String get invalidUsernamePassword {
    return Intl.message(
      'Invalid Username/Password',
      name: 'invalidUsernamePassword',
      desc: '',
      args: [],
    );
  }

  /// `Categories`
  String get categories {
    return Intl.message(
      'Categories',
      name: 'categories',
      desc: '',
      args: [],
    );
  }

  /// `Products`
  String get products {
    return Intl.message(
      'Products',
      name: 'products',
      desc: '',
      args: [],
    );
  }

  /// `Order List`
  String get orderList {
    return Intl.message(
      'Order List',
      name: 'orderList',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Cart List`
  String get cartList {
    return Intl.message(
      'Cart List',
      name: 'cartList',
      desc: '',
      args: [],
    );
  }

  /// `Customers`
  String get customers {
    return Intl.message(
      'Customers',
      name: 'customers',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ja'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}