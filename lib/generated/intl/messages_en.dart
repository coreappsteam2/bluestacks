// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addCustomer" : MessageLookupByLibrary.simpleMessage("Add customer"),
    "addPartner" : MessageLookupByLibrary.simpleMessage("Add partner"),
    "agentsAdded" : MessageLookupByLibrary.simpleMessage("Agents Joined"),
    "cartList" : MessageLookupByLibrary.simpleMessage("Cart List"),
    "categories" : MessageLookupByLibrary.simpleMessage("Categories"),
    "customers" : MessageLookupByLibrary.simpleMessage("Customers"),
    "enterPassword" : MessageLookupByLibrary.simpleMessage("Enter Password"),
    "enterUserEmail" : MessageLookupByLibrary.simpleMessage("Enter Email ID"),
    "enterUserFullname" : MessageLookupByLibrary.simpleMessage("Enter Name"),
    "enterUserMobile" : MessageLookupByLibrary.simpleMessage("Enter Mobile Number"),
    "enterUsername" : MessageLookupByLibrary.simpleMessage("Enter Username"),
    "flyingWolf" : MessageLookupByLibrary.simpleMessage("Dashboard"),
    "invalidUsernamePassword" : MessageLookupByLibrary.simpleMessage("Invalid Username/Password"),
    "loadingUsers" : MessageLookupByLibrary.simpleMessage("Loading Users"),
    "login" : MessageLookupByLibrary.simpleMessage("Login"),
    "orderList" : MessageLookupByLibrary.simpleMessage("Order List"),
    "partnersAdded" : MessageLookupByLibrary.simpleMessage("Partners joined"),
    "passwordError" : MessageLookupByLibrary.simpleMessage("Password should be more then 3 characters"),
    "pendingSales" : MessageLookupByLibrary.simpleMessage("Pending Sales"),
    "products" : MessageLookupByLibrary.simpleMessage("Products"),
    "referralCode" : MessageLookupByLibrary.simpleMessage("Referral Code"),
    "save" : MessageLookupByLibrary.simpleMessage("Save"),
    "search" : MessageLookupByLibrary.simpleMessage("Search"),
    "totalEarnings" : MessageLookupByLibrary.simpleMessage("Total Earnings"),
    "totalOrders" : MessageLookupByLibrary.simpleMessage("Total Orders"),
    "totalProducts" : MessageLookupByLibrary.simpleMessage("Total Products"),
    "totalSales" : MessageLookupByLibrary.simpleMessage("Total Sales"),
    "tournamentsPlayed" : MessageLookupByLibrary.simpleMessage("Tournaments \nPlayed"),
    "tournamentsWon" : MessageLookupByLibrary.simpleMessage("Tournaments \nWon"),
    "userMobileError" : MessageLookupByLibrary.simpleMessage("Mobile Number should be 10 digits"),
    "userfullnameError" : MessageLookupByLibrary.simpleMessage("Name should be more then 3 characters"),
    "usernameError" : MessageLookupByLibrary.simpleMessage("Please enter valid mobile number"),
    "viewAll" : MessageLookupByLibrary.simpleMessage("View All"),
    "winningPercentage" : MessageLookupByLibrary.simpleMessage("Winning \nPercentage"),
    "yourPartners" : MessageLookupByLibrary.simpleMessage("Your Partners")
  };
}
