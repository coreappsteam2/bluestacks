// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ja locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ja';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "enterPassword" : MessageLookupByLibrary.simpleMessage("パスワードを入力する"),
    "enterUsername" : MessageLookupByLibrary.simpleMessage("ユーザーネームを入力してください"),
    "flyingWolf" : MessageLookupByLibrary.simpleMessage("フライングウルフ"),
    "invalidUsernamePassword" : MessageLookupByLibrary.simpleMessage("無効なユーザー名/パスワード"),
    "loadingUsers" : MessageLookupByLibrary.simpleMessage("ユーザーの読み込み"),
    "login" : MessageLookupByLibrary.simpleMessage("ログイン"),
    "passwordError" : MessageLookupByLibrary.simpleMessage("パスワードは3文字以上にする必要があります"),
    "tournamentsPlayed" : MessageLookupByLibrary.simpleMessage("プレーしたトーナメント"),
    "tournamentsWon" : MessageLookupByLibrary.simpleMessage("トーナメントが勝ちました"),
    "usernameError" : MessageLookupByLibrary.simpleMessage("ユーザー名は3文字以上にする必要があります"),
    "winningPercentage" : MessageLookupByLibrary.simpleMessage("勝率\n"),
    "yourPartners" : MessageLookupByLibrary.simpleMessage("Your Partners")
  };
}
